---
# Display name
name: Jørgen Dale
avatar_image: "admin.jpg"
# Username (this should match the folder name)
authors:
- admin
# resume download button
btn:
- url : "files/cv.pdf"
  label : "Download Resume"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Mechatronics Engineer (M.Sc)

# Organizations/Affiliations
organizations:
- name: iDrop
  url: "https://thehub.io/startups/idrop-as"
- name:  MotionTech
  url: "https://motiontech.no/"
- name: YI2
  url: "https://yi2.no/"

# Short bio (displayed in user profile at end of posts)
bio: Solving problems you didn't know you had, in ways you can't understand
# Should the user's education and interests be displayed?
display_education: true

interests:
- Electronics
- Robotics
- Software developement
- 3D design and CAD
- 3D printing 

education:
  courses:
  - course: Mekatronikk M.Sc
    institution: University of Agder
    year: 2023
  - course: Mekatronikk B.Sc
    institution: University of Agder
    year: 2021
  - course: International Welding Engineer (IWE)
    institution: University of Agder
    year: 2020
  - course: Elektrofag
    institution: Bleiker VGS
    year: 2018
# Social/academia Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:joergen.dale@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/j%C3%B8rgendale/
# - icon: google-scholar
#   icon_pack: ai
#   link: https://
- icon: github
  icon_pack: fab
  link: https://github.com/dale837
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/dale837
# Link to a PDF of your resume/CV from the About widget.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "joergen.dale@gmail.com"
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

Eager mechatronics engineer with a background in electricity and electronics. I'm currently working full-time at iDrop, a startup focusing on developing autonomous robotics for offshore use. Both at work and at home I consistenly create and enhance skills within mechanical developement, battery packs, electronics, 3D-printing and more. 


<!-- ![reviews](img/certifacates.jpg) -->

