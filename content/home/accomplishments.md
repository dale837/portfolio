+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "Norsk Sveiseteknisk Forbund"
  organization_url = "https://www.sveis.no/"
  title = "International Welding Engineer"
  url = ""
  certificate_url = "files/IWE_diplom.pdf"
  date_start = "2020-01-01"
  date_end = "2020-12-01"
  description = "Complete course in welding-technical theory, methods, design and practice, as well as extensive material science."

[[item]]
  organization = "Univerity of Agder"
  organization_url = "https://www.uia.no/"
  title = "Bachelor's Thesis"
  url = ""
  certificate_url = "files/Development of a Portable Stewart Platform for Demonstrations and Educational Purposes.pdf"
  date_start = "2021-01-01"
  date_end = "2021-06-01"
  description = "Bachelor degree final thesis, on the Development of a Portable Stewart Platform for Demonstrations and Educational Purposes."

[[item]]
  organization = "Univerity of Agder"
  organization_url = "https://www.uia.no/"
  title = "Master's Thesis"
  url = ""
  certificate_url = "files/M_A_N_U_S_Project.pdf"
  date_start = "2023-01-01"
  date_end = "2023-06-01"
  description = "Master's degree final thesis, on the Design and Development of a low-cost Anthropomorphic Gripper for Service Robotics and Prosthetic Applications."

+++
