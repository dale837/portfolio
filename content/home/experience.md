+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = "ASSOCIATIONS THAT HELPED TO GATHER EXPERIENCE"

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[experience]]
  title = "Mechanical Developement Engineer"
  company = "iDrop"
  company_url = "https://thehub.io/startups/idrop-as"
  location = "Fredrikstad, Norway"
  date_start = "2023-08-01"
  date_end = ""
  description = """Working full-time as a developement engineer, designing and creating prototypes for electronics, batteries, actuation, valves and mechanical parts. I manage each part of the developement process, from design and part sourcing, to part production using 3D printing, moulding and machining, as well as assembly and testing.   
  """

[[experience]]
  title = "Mechatronics Engineer"
  company = "University of Agder"
  company_url = "https://www.uia.no/"
  location = "Grimstad, Norway"
  date_start = "2022-03-01"
  date_end = "2022-09-01"
  description = """Project on 3D printed robots and haptic feedback. I did the complete design and developement of a single-jointed, 3D printed robot arm. This included actuation and implementation of various feedback sensors.  
  """

[[experience]]
  title = "Mechatronics Engineer"
  company = "MotionTech"
  company_url = "https://motiontech.no/"
  location = "Grimstad, Norway"
  date_start = "2021-09-01"
  date_end = "2022-12-01"
  description = """Working as a part-time engineer alongside studies. I specialize in 3D design and simulation for the creation of digital twins for robotics. These projects involves software developement, control systems -and more.  
  """

[[experience]]
  title = "Consultant"
  company = "Young Industrial Innovators"
  company_url = "https://yi2.no/"
  location = "Grimstad, Norway"
  date_start = "2021-05-01"
  date_end = ""
  description = """Through Yi2 I have delivered several projects involving electronics and mechanical design, such as the complete design and developement of a mobile crop spaying platform for research purposes.   
  """

#[[experience]]
#  title = "CEO"
#  company = "Aviato"
#  company_url = ""
#  location = "Palo Alto, California"
#  date_start = "2009-02-01"
#  date_end = "2014-01-01"
# description = """Founder and CEO of Aviato, a fantastic Silicon Valley Startup
#  """

+++
