+++
# Gallery section using the Blank widget and Gallery element (shortcode).
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 66  # Order that this section will appear.

title = "Selected Projects"
subtitle = "Images from various personal, educational and commercial projects"

+++

{{< gallery >}}

Please see the [Projects]({{< ref "/post" >}} "Projects") section for more info about my projects.
