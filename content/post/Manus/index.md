---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "M.A.N.U.S"
subtitle: "***Master's Project*** --- Design and Development of a low-cost Anthropomorphic Gripper for Service Robotics and Prosthetic Applications"
summary: "***Master's Project*** - Design and Development of a low-cost Anthropomorphic Gripper for Service Robotics and Prosthetic Applications"
authors: 
- admin
tags: []
categories: []
date: 2024-11-27T18:04:50+01:00
lastmod: 2024-11-27T18:04:50+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []

gallery_item:
- album: gallery
  caption: Default
  image: 04Apple.png

---

This project is associated with my Master's project on the design, development and evaluation of a low-cost anthropomorphic gripper for advanced robotic applications. The project involves the creation of a fully functional robotic gripper through 3D printing and assembly, as well as the creation of custom software and frameworks for controlling the device. One of these frameworks is the ability to mimic human hand movements in real time using hand tracking algorithms. Another is the successful integration of the gripper with a collaborative robotic arm from Universal Robots, leveraging ROS2 for seamless communication and coordination. The system demonstrates the capability to pick and place operations with diverse objects through the incorporation of state-of-the-art computer vision model YOLOv8. Additionally, the integration with Moveit 2 gives the system advanced motion planning and control capabilities. The results of this research indicates the potential for improved dexterity and adaptability in robotic manipulation tasks without costly hardware, paving the way for further advancements in human-robot interaction and automation.

{{< gallery >}}
{{< youtube G58Zcq2uR0M >}}
