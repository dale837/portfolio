---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Autonomous Pick-and-Place Robot"
subtitle: "Complete software developement for an autonomous, differentially driven robot capable of locating and picking up objects using camera vision and machine learning"
summary: "Complete software developement for an autonomous, differentially driven robot capable of locating and picking up objects using camera vision and machine learning"
authors:
- admin
tags: []
categories: []
date: 2024-11-27T18:04:50+01:00
lastmod: 2024-11-27T18:04:50+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []


---

This project is based on the recently developed robot
platform called the UiABot. The following report encompasses
the work and research related to developing software for the
autonomous behavior of the robot. In addition the already
implemented motion system of the robot platform, a camera-
manipulator system is created to enable autonomous Pick and
Place operations. Within the scope of the project, the principles of the autonomous system will be proposed and tested. An experimental setup is developed, and used to its full potential. Several machine learning models are put to the test, and a framework for object detection is implemented. This framework allows for localization of the detected object, and will enable the motion platform to move to an object’s location and perform a Pick and Place operation using the onboard manipulator.

{{< gallery >}}
{{< youtube 2tdZqYn54gk >}}

