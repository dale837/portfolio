---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Stewart Platform"
subtitle: "***Bachelor's Project*** --- Developement of a Portable Stewart Platform for Educational Purposes"
summary: "***Bachelor's Project*** --- Developement of a Portable Stewart Platform for Educational Purposes"
authors:
- admin
tags: []
categories: []
date: 2024-11-30T15:08:49+01:00
lastmod: 2024-11-30T15:08:49+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

This project consisted of the further development of a portable Stewart platform on behalf of the University of Agder (UiA). The finished result has the purpose of being used as demonstration unit for presenting mechatronics or the Motion Lab. The project featured in this report is a continuation of a Stewart platform created as a bachelor’s project in 2020. The 2020 project produced a functional small-scale Stewart Platform, using small linear actuators and consumer electronics. The efforts in this report is aimed at further developing this small-scale Stewart platform. That includes the creation of a a system capable of smooth motion in all degrees of freedom, as well as a user interface to control it. To achieve this, a new control system and a graphical user interface has been designed and implemented. This required some new components, such as new actuators and upgraded electronics. To accommodate this new hardware, some components had to be replaced or remodelled, although the main mechanical structure remains unaltered. These efforts resulted in a fully embedded, portable Stewart platform, capable of moving smoothly in all six degrees of freedom.

{{< gallery >}}
{{< youtube cWbvQRoC-gM >}}
