---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Nibio Project"
subtitle: "Design and construction of a spraying-rig for test fields"
summary: "Design and construction of a spraying-rig for test fields"
authors:
- admin
tags: []
categories: []
date: 2024-11-27T18:04:50+01:00
lastmod: 2024-11-27T18:04:50+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: "center"
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []


---

{{< gallery >}}



